# README #

### What is this repository for? ###

* This repositories provide **eztq** (standing for easy_torque), which enables you to run your torque jobs easily (without creating sub files)!
* Version: beta

### How do I get set up? ###
eztq requires **Python3**.
```
$ git clone git@bitbucket.org:otita/easy_torque.git
$ cd easy_torque
$ chmod +x eztq #make eztq executable
```
After this, you can move eztq to somewhere in $PATH.

### How to use ###
This repository includes a very simple example of eztq.
In *example* directory, you can find two files **example.py** and **example.json**.
example.py is a very simple python program, and you can run it by
```
$ python3 example.py --count 2 --string foo output.txt
```
and it will create output.txt like
```
foo
foo
```

Let's consider the case you want to run example.py with various arguments in torque.
Do you want to create a lot of sub files? (**No way!**) Then you want to use **eztq**!

eztq requires you to create a json file in which the definitions of jobs are written.
You can find **example.json** in *example* directory like
```
{
  "job1": {
    "BASE": "python3 example.py",
    "NAMED_ARGS": {
      "count": [1, 2, 3, 4],
      "string": ["foo{count}", "bar{count}"]
    },
    "UNNAMED_ARGS": {
      "ORDER": ["output_path"],
      "output_path": "./{count}_{string}.txt"
    },
    "NAME": "{count}_{string}",
    "STDOUT": "{count}_{string}.out",
    "STDERR": "/dev/null",
    "RESOURCE": "nodes=1:ppn=4"
  }
}
```
In this file, only one job **job1** is defined (of course, you can add more job definitions).
You can run **job1** by
```
$ eztq -f sample.json job1
```
or dryrun it (not post jobs in torque) by
```
$ eztq -d -f sample.json job1
```
for tests.

Then eztq automatically post your jobs in torque with all the possible combinations of arguments. Each job is like
```
#PBS -N 1_foo1
#PBS -o 1_foo1.out
#PBS -e /dev/null
#PBS -l nodes=1:ppn=4
python3 example.py --count 1 --string foo1 ./1_foo1.txt
```
## That's it! ##

### Contribution guidelines ###
* ...