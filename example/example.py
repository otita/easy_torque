#!/usr/bin/env python3
# -*- coding: utf-8 -*-
def main():
  args = get_args()
  output_path = args["output_path"]
  count = int(args["count"])
  string = args["string"]

  with open(output_path, mode="w") as f:
    for i in range(count):
      f.write(string + "\n")

def get_args():
  import argparse
  parser = argparse.ArgumentParser()
  parser.add_argument("output_path")
  parser.add_argument("-c", "--count", required=True)
  parser.add_argument("-s", "--string", required=True)
  return vars(parser.parse_args())

if __name__ == '__main__':
  main()
